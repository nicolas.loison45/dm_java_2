import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer minimum = null;
        if (liste.size() != 0){
          minimum = liste.get(0);
        }
        for (Integer i : liste){
          if (minimum > i){
            minimum = i;
          }
        }
        return minimum;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeur donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean ok = true;
        int i = 0;
        if (liste.size()==0){
          return ok;
        }
        else{
          while (ok && i<liste.size()){
            System.out.println("("+ valeur +liste.get(i)+ valeur.compareTo(liste.get(i)));
            if (valeur.compareTo(liste.get(i)) >= 0){
              ok = false;
            }
            i+=1;
          }
        }
        return ok;
    }

    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> liste = new ArrayList<>();
        for (T elem : liste1){
          for (T elem2 : liste2){
            if (elem.equals(elem2) && !liste.contains(elem)){
              liste.add(elem);
            }
          }
        }
        return liste;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        String separateur = " ";
        List<String> liste = new ArrayList<>();
        if (texte.length()==0){
          return liste;
        }
        else {
          for (String s : texte.split(separateur)){
            if (!s.equals("")){
                liste.add(s);
            }
          }
        }
        return liste;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
      if (texte.length()==0){
        return null;
      }
      List<String> liste = new ArrayList<>();
      liste = BibDM.decoupe(texte);
      int max = Collections.frequency(liste, liste.get(0));
      String mot = liste.get(0);
      for (int i =1;i<liste.size()-1;i++){
        if (Collections.frequency(liste, liste.get(i))>=max){
            mot = liste.get(i);
        }
      }
      return mot;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        return true;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        return true;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      int debut = 0;
      int fin = liste.size()-1;
      int milieu;
      boolean trouve = false;
      while (!trouve && (debut <= fin)){
        milieu = (debut + fin) / 2;
        if (valeur > liste.get(milieu)){
          debut = milieu + 1;
        }
        else if (valeur == liste.get(milieu)){
          trouve = true;
        }
        else {
          fin = milieu - 1;
        }
      }
      return trouve;
    }



}
